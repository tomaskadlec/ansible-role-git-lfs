ansible-role-git-lfs
=====================

.. _`Ansible`: https://ansible.com
.. _`git-lfs`: https://git-lfs.github.com/ 

The project installs `git-lfs`_ on supported systems. Linux on ``x86_64``
platform is the default one. It uses binary provided by the `git-lfs`_ project.

Requirements
------------

* `Ansible`_ 2.2 and newer
* tar, gzip, bzip2

Installation
------------

There are two possibilities how to install the roles. Choose one that suits you best.

#. Copy the role into the ``roles`` subdirectory: ::

    cd YOUR_PROJECT
    mkdir -p roles/swarm
    wget -O - \
        https://gitlab.com/tomaskadlec/ansible-role-git-lfs/repository/archive.tar.bz2?ref=master \
        | tar xjf - --wildcards -C roles/git-lfs --strip=1

#. If your project uses git you can make use of git submodules: ::

     cd YOUR_PROJECT
     mkdir -p roles
     git submodule add git@gitlab.com:tomaskadlec/ansible-role-git-lfs.git roles/git-lfs

Configuration
-------------

.. _`defaults/main.yml`: defaults/main.yml
.. |defaults/main.yml| replace: ``defaults/main.yml``   

`git-lfs` is by default installed to ``/usr/local/git-lfs`` and symlinked to
``/usr/local/bin/git-lfs``. If you want to change it provide your own
configuration values. Look into |defaults/main.yml|_ for reference.

Please note that ``hash_behavior`` must be set to ``merge``. Otherwise the
recipe above won't work (variable ``git_lfs`` will be overwritten).

Usage
-----

Just include role in your playbook. It will try to autodetect version you are
using currently. If it is necessary it will install or update `git-lfs`_.

::

    -   name: "Install git-lfs"
        hosts:  [all]
        roles:
            - role: git-lfs


.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
